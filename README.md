# iOS Low-Power Mode Detect

Small library that tries to detect low power mode on iOS devices from within a browser.
It works by trying to auto-play a invisible video file. Video auto-play is one of the features that is disabled on iOS devices when low-power mode is enabled.

## Usage

Download the library file(s) from the `dist/` folder and add them to your project.
Load the library preferrably at the bottom of your html file (only the minified `ioslpd.min.js` is needed):

```html
  ...
  <script src="./path/to/ioslpd.min.js"></script>
  <script>...</script>
</body>
```

Loading the library file will add the global object `ioslpd` with the following properties:

### `ioslpd.isIosDevice`
A boolean value, `true` if the current device is an iOS device, `false` otherwise.

The detection is made based on the browser's user agent string.

### `ioslpd.testPowerMode`
A Javascript promise that tests if low-power mode is enabled or not.

Resolve the promise with `.then(resolved, rejected)` and pass two functions for when it is resolved (low-power mode detected) and when it is rejected ("normal" power mode).

Examples:

```html
  ...
  <script src="./path/to/ioslpd.min.js"></script>
  <script>
    if (ioslpd.isIosDevice) {
      console.log('iOS device detected!');

      ioslpd.testPowerMode.then(
        function() { console.log('🪫 low power mode detected!') },
        function() { console.log('🔋 normal power mode') }
      );
    }
  </script>
</body>
```