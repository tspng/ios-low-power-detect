import { terser } from "rollup-plugin-terser";
import { getBabelOutputPlugin } from "@rollup/plugin-babel";

const defaultOuputOptions = { sourcemap: true, format: "iife", name: "ioslpd" };

export default {
  input: "src/main.js",
  watch: {
    include: "./src/**",
  },
  output: [
    {
      file: "dist/ioslpd.js",
      ...defaultOuputOptions,
    },
    {
      file: "dist/ioslpd.min.js",
      plugins: [
        getBabelOutputPlugin({
          presets: ["@babel/preset-env"],
          allowAllFormats: true,
        }),
        terser(),
      ],
      ...defaultOuputOptions,
    },
  ],
};
